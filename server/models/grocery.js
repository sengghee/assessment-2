module.exports = function(conn, Sequelize) {
//    console.log("In here.....");
    var Grocerylist =  conn.define('grocery_list', {
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },

    }, {
        tableName: 'grocery_list',
        timestamps: false
    });
    return Grocerylist;
};