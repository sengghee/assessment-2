(function () {
    angular
        .module("GroceryApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http","$state"];

    function SearchCtrl($http,$state) {
        var vm = this;

        vm.brand = '';
        vm.name = '';
        vm.result = null;
        vm.showManager = false;

        // Exposed functions
        vm.search = search;
        vm.searchForManager = searchForManager;

        // Initial calls
        init();


        // Function definitions
        function init() {
            console.log("In init.....");
            $http
                .get("/api/grocerys")
                .then(function (response) {
                    console.log ("in init sucess"+ response.data);

                    vm.departments = response.data;
                })
                .catch(function (err) {
                    console.log ("in init err");
                    cosole.log("error " + err);
                });
        }

        function search() {
            vm.showManager = false;

            $http
                .get("/api/grocerys",
                    {
                        params: {
                            'brand': vm.brand,
                            'name' : vm.name
                        }
                    }
                )
                .then(function (response) {
                    console.log("In search function sucess..........");
                    console.log ("in init sucess"+ response.data)
                    vm.departments = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }

        function searchForManager() {
            vm.showManager = true;
            $http.get("/api/managers",
                {
                    params: {
                        'dept_name': vm.deptName
                    }
                })
                .then(function (response) {
                    vm.departments = response.data;
                    console.log("vm.departments content: " + JSON.stringify(response.data));
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        };

        vm.showManagerFunction=function (empNo) {
            console.log ("Inside showManagerFunction " + empNo);
            var clickedEmpNo=empNo  ;
            $state.go("C1",{empNo : clickedEmpNo});
        }
    };

})();