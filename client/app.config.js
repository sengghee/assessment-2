/**
 * Created by Seng Ghee on 2/11/2016.
 */

(function () {
    angular
        .module("GroceryApp")
        .config(groceryAppConfig)
    groceryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function groceryAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'./insert/insert.html',
                controller : 'InsertCtrl',
                controllerAs : 'ctrl'
            })
            .state('B',{
                url : '/B',
                templateUrl :'./search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('C',{
                url : '/C',
                templateUrl :'./show/show.html',
                controller : 'ShowCtrl',
                controllerAs : 'ctrl'
            })
            .state('C1',{
                url : '/C1/:empNo',
                templateUrl :'./show/show.html',
                controller : 'ShowCtrl',
                controllerAs : 'ctrl'
            })

        $urlRouterProvider.otherwise("/A");


    }
})();
